'''
@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics, Inc.
@create data: Oct 1, 2021

FHIR Bulk Export

'''
import time, sys
import logging

from fhir_api import GoogleApi
from config import BULK_SLEEP_TIME, MAX_BULK_REQUESTS, LOG_FILE, FHIR_SERVER

if __name__ == '__main__':
    logging.basicConfig(filename=LOG_FILE, 
                    level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    filemode='a')

    # command args
    operation_name = None
    operation_id = None
    download_only = True
    if len(sys.argv) > 1:
        if sys.argv[1] == '-export':
            download_only = False
        elif sys.argv[1] == '-download':
            if len(sys.argv) > 2:
                operation_id = sys.argv[2] if sys.argv[2] else None
            else:
                print('Operation id required')
                print('--download <operation-id>')
                sys.exit()
    else:
        print('Arguments required:')
        print('-export')
        print('or')
        print('-download <operation-id>')
        sys.exit()

    # Get client FHIR API
    if FHIR_SERVER == 'Google':
        fhir_api = GoogleApi()
    else:
        print('Invalid FHIR server')
        sys.exit()

    # Bulk kick-off request
    request_done = False
    if not download_only:
        logging.info('Started bulk kick-off request...')
        print('Started bulk kick-off request...')
        operation_name = fhir_api.bulk_kickoff()
        if not operation_name:
            logging.error('Error: bulk kick-off request failed!')
            sys.exit()
    
        # wait until bulk request is done
        n = 0
        while not request_done and n < MAX_BULK_REQUESTS:
            time.sleep(BULK_SLEEP_TIME)
            request_done = fhir_api.bulk_status(operation_name)
            n +=1

    # download resources
    if request_done or download_only:
        logging.info('downloading resources...')
        print('downloading resources...')
        resources = fhir_api.fhir_resources
        for resource in resources:
            if fhir_api.download_resource(resource, operation_name, operation_id):
                print('{}'.format(resource))
    else:
        logging.error(
                'Bulk request operation {0} did not complete afer {1} seconds'.format(
                                                            operation_name,
                                                            MAX_BULK_REQUESTS*BULK_SLEEP_TIME,
                                                            ))

    logging.info('done')
