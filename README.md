# FHIR Bulk Export

Installation on Ubuntu 18.04 or CentSO 7 with ESP.

## Installation

1. change to esp user and change to /srv/esp directory. 
```
sudo su esp
cd /srv/esp
```

2. Clone FHIR repo 
```
git clone https://gitlab.com/ESP-Project/fhir.git fhir 
```

3. Make data directory for FHIR resource download files.
```
mkdir /srv/esp/fhir/data
```

4. Install Google Cloud SDK (for Google FHIR server)

Install Cloud SDK 
https://cloud.google.com/sdk/docs/install#deb

Then execute the following two commands at the command line to establish your user credentials:
```
gcloud auth login
gcloud auth application-default login
```

5. Create a Python virtual environment in ESP directory.

cd /srv/esp
python3 -m venv fhir_venv

6. Install Google cloud python packages.


```
source /srv/esp/fhir_venv/bin/activate
pip install google-api-python-client
pip install google-cloud-storage
```

## Configuration

Copy the sample configuration file and edit paramters. Some parmaters are specific 
to a Google FHIR server and more parameters can be added as needed for other FHIR servers.

```
cp config.py.sample config.py
vi config.py
```

## Run FHIR Bulk Export

1. Start bulk kick-off request and download resource files

```
source /srv/esp/fhir_venv/bin/activate
python fhir_export.py -export
```

2. Or, only download resource files.

This is only used to download existing resource files from the FHIR server, 
and does not execute a bulk kick-off request. 

```
source /srv/esp/fhir_venv/bin/activate
python fhir_export.py -download <operation_id>
```

The operation_id is specific to Google and is the prefix added to each resource object 
in a Google storage cloud bucket for the bulk export.

example: 
```
source /srv/esp/fhir_venv/bin/activate
python fhir_export.py -download 12719670076064661505 
```

## Copy bulk files into the ESP data directory

Use the following command to copy the bulk files into the ESP data directory.

```
source /srv/esp/fhir_venv/bin/activate
python copy_fhir_esp.py
```

Note: Copied files are renamed using the naming convention used by the 
ESP FHIR loader utility in the next step.

## Load FHIR data files into the ESP database

First validate files.  

```
cd /srv/esp/prod
bin/esp load_fhir --validate
```

Then load files into the ESP database.  


```
bin/esp load_fhir
```
