#! /usr/bin/env bash

# Run FHIR Bulk export
python3 fhir_export.py -export

# copy bulk files into ESP data directory
echo "Copying bulk files into ESP data directory."
python3 copy_fhir_esp.py
echo "done"

