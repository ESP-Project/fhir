'''
@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics, Inc.
@create data: Oct 14, 2021

Cleans and moves FHIR Bulk Export resources to ESP load directory.

'''
import datetime, os.path, json
import logging
from shutil import move
from config import ESP_DATA_DIR, DOWNLOAD_DIR, LOG_FILE

def parse_observation(file, download_dir):
    """
    Parse observation file into seperate files: vital-signs, lab results, and 
    social history.
    """
    with open(file, mode='r') as obs_file, \
         open(download_dir + 'vitals.ndjson', mode='w') as vitals, \
         open(download_dir + 'labres.ndjson', mode='w') as labres, \
         open(download_dir + 'sochist.ndjson', mode='w') as sochist, \
         open(download_dir + 'obs_error.ndjson', mode='w') as obs_error:
        for line in obs_file:
            observation = json.loads(line)
            if 'category' in observation:
                category = observation['category'][0]['coding'][0]['code']
                not_valid = '\\' in line
                if not_valid: # lines that cauxe json errors
                    obs_error.write(line)
                elif category == 'vital-signs':
                    vitals.write(line)
                elif category == 'laboratory':
                    labres.write(line)
                elif category == 'social-history':
                    sochist.write(line)
                else:
                    obs_error.write(line)
            else:
                obs_error.write(line)
    os.remove(file)

def process_condition(file, download_dir):
    '''Removes all coding systems except http://hl7.org/fhir/sid/icd-10-cm.
    '''
    with open(file, mode='r') as cond_input, open(download_dir + 'cond.ndjson', mode='w') as cond_output:
        for line in cond_input:
            condition = json.loads(line)
            code = condition['code']['coding']
            for c in code:
                if 'system' in c and c['system'] == "http://hl7.org/fhir/sid/icd-10-cm":
                    code.clear()
                    code.append(c)
                    cond_output.write(json.dumps(condition)+ '\n')
                    break
    os.remove(file)

def process_coverage(file, download_dir):
    '''Removes all coding systems except http://terminology.hl7.org/CodeSystem/v3-ActCode.
    '''
    with open(file, mode='r') as covg_input, open(download_dir + 'covg.ndjson', mode='w') as covg_output:
        for line in covg_input:
            coverage = json.loads(line)
            type = coverage['type']
            if 'coding' in type:
            	code = type['coding']
            	for c in code:
                    if 'system' in c and c['system'] == "http://terminology.hl7.org/CodeSystem/v3-ActCode":
                    	code.clear()
                    	code.append(c)
                    	covg_output.write(json.dumps(coverage)+ '\n')
                    	break
    os.remove(file)

def process_medication(file, download_dir):
    '''Removes characters that cause json errors (escape char etc.).
    '''
    with open(file, mode='r') as med_input, open(download_dir + 'med.ndjson', mode='w') as med_output:
        for line in med_input:
            # remove characters that cause json errors
            line = line.replace('\\','')# escaped char
            med_output.write(line)

if __name__ == '__main__':

    logging.basicConfig(filename=LOG_FILE, 
                    level=logging.INFO,
                    format='%(asctime)s %(message)s',
                    filemode='a')

    resources = {
            'fhircvg':'covg',
            'fhircnd':'cond',
            'fhirimm':'Immunization',
            'fhirmed':'MedicationRequest',
            'fhirmd1':'med',
            'fhirmem':'Patient',
            'fhirobs':'vitals',
            'fhirres':'labres',
            'fhirsoc':'sochist',
            'fhirvis':'Encounter'
        }
    logging.info('Started processing FHIR files...')
    # Process coverage file
    cvg_file = DOWNLOAD_DIR + 'Coverage.ndjson'
    if os.path.isfile(cvg_file):
        logging.info('Processing {}'.format(cvg_file))
        process_coverage(cvg_file,DOWNLOAD_DIR)
    # Process condition file
    cnd_file = DOWNLOAD_DIR + 'Condition.ndjson'
    if os.path.isfile(cnd_file):
        logging.info('Processing {}'.format(cnd_file))
        process_condition(cnd_file,DOWNLOAD_DIR)
    # Parse and clean observation file
    obs_file = DOWNLOAD_DIR + 'Observation.ndjson'
    if os.path.isfile(obs_file):
        logging.info('Processing {}'.format(obs_file))
        parse_observation(obs_file, DOWNLOAD_DIR)
    # Clean Medication file
    med_file = DOWNLOAD_DIR + 'Medication.ndjson'
    if os.path.isfile(med_file):
        logging.info('Processing {}'.format(med_file))
        process_medication(med_file,DOWNLOAD_DIR)
    # Move files into ESP load directory
    logging.info('Moving files to ESP directory...')
    for key, val in resources.items():
        src_file = DOWNLOAD_DIR + val + '.ndjson'
        today = datetime.date.today()
        today_str = today.strftime('%m%d%Y')
        dst_file = ESP_DATA_DIR + key + '.esp.' + today_str
        if os.path.isfile(src_file):
            if (os.stat(src_file).st_size > 0):
                logging.info('Moving {0} to {1}'.format(src_file,dst_file))
                move(src_file, dst_file)
            else:
                print('File {} is empty.'.format(src_file))
                logging.error('File {} is empty.'.format(src_file))
        else:
            print('File {} does not exist.'.format(src_file))
            logging.info('File {} does not exist.'.format(src_file))

    logging.info('done')
