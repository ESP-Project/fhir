'''
@author: Jeff Andre <jandre@commoninf.com>
@organization: Commonwealth Informatics, Inc.
@create data: Oct 1, 2021

Client FHIR API

'''
import logging
from abc import ABC, abstractmethod

from googleapiclient import discovery
from google.cloud import storage, exceptions
from config import (API_VERSION, SERVICE_NAME, PROJECT_ID, LOCATION, DATASET_ID, 
        FHIR_STORE_ID, GCS_BUCKET, GCS_BUCKET_DIR, DOWNLOAD_DIR, FHIR_RESOURCES)

class AbstractApi(ABC):
    """ Abstract class for FHIR client API.
    """

    # Required methods
    @abstractmethod
    def bulk_kickoff(self):
        pass
    @abstractmethod
    def bulk_status(self):
        pass
    @abstractmethod
    def download_resource(self):
        pass

class ClientApi(AbstractApi):
    """ Base class for FHIR client API.
    """

    download_dir = DOWNLOAD_DIR
    fhir_resources = FHIR_RESOURCES

class GoogleApi(ClientApi):
    """Class to export FHIR resources from Google Cloud.
    """

    api_version = API_VERSION
    service_name = SERVICE_NAME
    project_id = PROJECT_ID
    location = LOCATION  
    dataset_id = DATASET_ID 
    fhir_store_id = FHIR_STORE_ID 
    gcs_bucket = GCS_BUCKET
    gcs_bucket_dir = GCS_BUCKET_DIR

    # Instantiates an authorized API client by discovering the Healthcare API
    # and using GOOGLE_APPLICATION_CREDENTIALS environment variable.
    client = discovery.build(service_name, api_version)
    
    def bulk_kickoff(self):
        """Exports resources to a Google Cloud Storage bucket from the FHIR store.
        """
        fhir_store_parent = "projects/{}/locations/{}/datasets/{}".format(
            self.project_id, self. location, self.dataset_id
            )
        fhir_store_name = "{}/fhirStores/{}".format(fhir_store_parent, self.fhir_store_id)

        body = {"gcsDestination": {"uriPrefix": "gs://{}/{}".format(self.gcs_bucket, self.gcs_bucket_dir)}}
        request = self.client.projects().locations().datasets().fhirStores().export(name=fhir_store_name, body=body)
        response = request.execute()
        name = response['name'] if response else None
        operation_name = name if name else None
        if operation_name:
            logging.info("Exporting FHIR resources to bucket: gs://{}/{}".format(self.gcs_bucket, self.gcs_bucket_dir))
        else:
            logging.error("Error: Failed to export FHIR resources to bucket: gs://{}/{}".format(self.gcs_bucket, self.gcs_bucket_dir))

        return operation_name

    def bulk_status(self, operation_name):
        """ Get operation status for a given operation id
        """
        request = self.client.projects().locations().datasets().operations().get(name=operation_name)
        response = request.execute()
        request_done = response['done'] if response and ('done' in response) else False
        return request_done

    def download_resource(self, resource, operation_name, operation_id=None):
        """Downloads a resource (blob) from the bucket into a file."""
        storage_client = storage.Client(project=self.project_id)
        bucket = storage_client.bucket(self.gcs_bucket)

        # Construct a client side representation of a blob.
        if not operation_id and operation_name:
            operation_id = operation_name.split('/')[-1] if operation_name else None
        object_name = operation_id + '_' + resource
        blob_name = self.gcs_bucket_dir + object_name
        blob = bucket.blob(blob_name)

        # download blob to local file
        file_name = resource + '.ndjson'
        file_path = self.download_dir + file_name
        try:
            blob.download_to_filename(file_path, checksum=None)
            logging.info(
                "Downloaded storage object {} from bucket {} to file {}.".format(
                        blob_name, self.gcs_bucket, file_path
                ))
            return True
        except exceptions.NotFound:
            logging.error("Error: GCS object {} not found in bucket {} for project {}.".format(blob_name, self.gcs_bucket, self.project_id))

        except Exception as e:
            logging.error("Error: access issue with bucket {} for project {}.".format(self.gcs_bucket, self.project_id))
            logging.error(e)
            return False

